package com.certussolutions.filesplitter;

public class FileSplittConfig {
    public String filePath;
    public int lines;
    public int headerLines = 1;
}
