package com.certussolutions.filesplitter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public final class App {

    public static void main(String args[]) {

        FileSplittConfig config = parseArgs(args);

        System.out.println("Attempting to split: " + config.filePath + " into " + config.lines + " files");

        try {
            int lineCount = 0;
            File file = new File(config.filePath);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) { // counting the lines in the input file
                scanner.nextLine();
                lineCount++;
            }

            scanner.close();

            System.out.println("Total Lines in file: " + lineCount);

            int files = 0;
            if (((lineCount - 1) % config.lines) == 0) {
                files = ((lineCount - 1) / config.lines);
            } else {
                files = ((lineCount - 1) / config.lines) + 1;
            }
            System.out.println("Files to be created: " + files); // number of files that shall be created

            BufferedReader reader = new BufferedReader(new FileReader(config.filePath));

            // Get the first n lines
            List<String> headerLines = new ArrayList<String>();
            for (int i = 0; i < config.headerLines; i++) {
                headerLines.add(reader.readLine());
            }

            for (int i = 1; i <= files; i++) {
                FileWriter fstream1 = new FileWriter("FileNumber_" + i + ".csv"); // creating a new file writer.
                BufferedWriter out = new BufferedWriter(fstream1);
                for (String headerLine : headerLines) {
                    out.write(headerLine);
                    out.newLine();
                }

                System.out.println("\nCreating file number " + i);

                int linesWritten = 0;
                for (int j = 0; j < config.lines; j++) {
                    String line = reader.readLine();
                    if (line != null) {
                        out.write(line); // acquring the first column
                        out.newLine();
                        linesWritten++;
                    }
                }
                out.close();
                System.out.println("File " + i + " was created with " + linesWritten + " lines");
            }
            reader.close();
            System.out.println("File splitter process completed!");

        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static FileSplittConfig parseArgs(String[] args) {
        if (args.length < 2 || args.length > 3) {
            throw new IllegalArgumentException(
                    "must have at least 2 args: 'file path',  'number of files' and optionally 'number of header lines'");
        }
        FileSplittConfig config = new FileSplittConfig();
        config.filePath = args[0];
        config.lines = Integer.parseInt(args[1]);
        if (args.length == 3) {
            config.headerLines = Integer.parseInt(args[2]);
        }
        return config;
    }

}